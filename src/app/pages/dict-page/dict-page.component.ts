import { Component, OnInit } from '@angular/core';
import { ConectionService } from 'src/app/services/conection.service';

@Component({
  selector: 'app-dict-page',
  templateUrl: './dict-page.component.html',
  styleUrls: ['./dict-page.component.css']
})
export class DictPageComponent implements OnInit {

  lat: number = 2.3933352;
  lon: number = -75.2482212;
  cards: any;

  constructor(private conection: ConectionService) { }

  ngOnInit() {
    this.conection.listCards().subscribe( cards => {
      this.cards = cards;
    });
  }

  goCard(link) {
    window.open(link, '_blank');
  }

}
