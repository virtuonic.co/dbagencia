import { Component, OnInit } from '@angular/core';
import { ConectionService } from 'src/app/services/conection.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  items: any;

  constructor(private conection: ConectionService) { }

  ngOnInit() {
    this.conection.listItems().subscribe(items => {
      this.items = items;
      this.items.sort((a,b) => (a.fecha < b.fecha) ? 1 : ((b.fecha < a.fecha) ? -1 : 0));
      var desc = this.items[0].descripcion;
      desc = desc.substring(0, 200) + "..."
      this.items[0].descripcion = desc;
    });
  }

}
