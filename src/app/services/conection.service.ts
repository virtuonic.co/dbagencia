import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

export interface Item { 
  titulo: string; 
  subtitulo: string;  
  fecha: string;
  imagen: string;
  descripcion: string;
  filename: string;
  userUID: string;
}

export interface Card { 
  nombre: string;
  bio: string;
  link: string;
  imagenURL: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConectionService {

  private itemsCollection: AngularFirestoreCollection<Item>;
  private cardsCollection: AngularFirestoreCollection<Card>;

  constructor(private afs: AngularFirestore) { 
    this.itemsCollection = afs.collection<Item>('items');
    this.cardsCollection = afs.collection<Card>('dbcards');
  }

  listItems() {
    return this.itemsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Item;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

  listCards() {
    return this.cardsCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Card;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }
}
